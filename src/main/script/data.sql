insert into product_detail (id, color, description, price) values (1, 'blue', 'blue wallet', 23);
insert into product (id, created_at, name, stock, product_detail_id) values (1, '2023-08-30T10:54:50.688', 'green wallet', 23, 1);
insert into command (id, created_at, customer_id) values (1, '2023-08-30T10:54:50.688', 1);

insert into product_detail (id, color, description, price) values (2, 'red', 'red shoes', 12);
insert into product (id, created_at, name, stock, product_detail_id) values (2, '2023-08-30T10:54:50.688', 'red shoes', 23, 2);
insert into command (id, created_at, customer_id) values (2, '2023-08-30T10:54:50.688', 2);
insert into command_product (command_id, product_id)  values(2, 2);