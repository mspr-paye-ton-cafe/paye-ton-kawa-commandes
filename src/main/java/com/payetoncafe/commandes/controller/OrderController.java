package com.payetoncafe.commandes.controller;

import com.payetoncafe.commandes.api.OrdersApi;
import com.payetoncafe.commandes.exception.OrderException;
import com.payetoncafe.commandes.model.OrderWithProducts;
import com.payetoncafe.commandes.model.Orders;
import com.payetoncafe.commandes.service.OrderService;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class OrderController implements OrdersApi {
    @Inject
    OrderService orderService;
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);


    @Override
    public ResponseEntity<Orders> createOrder(Orders orders) {
        return ResponseEntity.ok(orderService.createOrder(orders));
    }

    @Override
    public ResponseEntity<Void> deleteOrder(Integer orderId) {
        try {
            orderService.deleteOrder(orderId);
            return ResponseEntity.ok().build();
        } catch (OrderException e) {
            log.info("Error while deleting order");
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }


    @Override
    public ResponseEntity<List<OrderWithProducts>> getAllOrders() {
        try {
            List<OrderWithProducts> order = orderService.getOrders();
            log.info(String.format("Response Ok, ordersList :  %s", order));
            return ResponseEntity.ok(order);
        } catch (OrderException e){
            log.info("Error while getting order");
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }


    @Override
    public ResponseEntity<OrderWithProducts> getOrderById(Integer orderId) {
        try {
            OrderWithProducts orderWithProducts = orderService.getOrderById(orderId);
            return ResponseEntity.ok(orderWithProducts);
        } catch (OrderException e){
            log.info(String.format("Error getting command by id %s %s ", orderId, ", it does not exist"));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Orders> updateOrder(Orders orders) {
        try {
            return ResponseEntity.ok(orderService.updateOrder(orders));
        } catch (OrderException e){
            log.info("Error while updating order");
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }

}
