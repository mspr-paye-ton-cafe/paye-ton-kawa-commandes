package com.payetoncafe.commandes.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderProducer {

    private static final Logger log = LoggerFactory.getLogger(OrderProducer.class);

    public static final String TOPIC = "orderStock";

    private final KafkaTemplate<String, List<Integer>> kafkaTemplate;

    public OrderProducer(KafkaTemplate<String, List<Integer>> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendOrderStockEvent(List<Integer> productIds){
        log.info("Kafka : Début Envoi des stocks produits à mettre à jour");
        kafkaTemplate.send(TOPIC, productIds);
        log.info("Kafka : Fin Envoi des stocks produits à mettre à jour");
    }
}
