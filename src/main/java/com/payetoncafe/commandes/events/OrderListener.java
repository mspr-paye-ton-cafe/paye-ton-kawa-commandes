package com.payetoncafe.commandes.events;

import com.payetoncafe.commandes.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class OrderListener {

    private final OrderRepository orderRepository;

    private static final Logger log = LoggerFactory.getLogger(OrderListener.class);

    public OrderListener(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @KafkaListener(topics = "customerDelete", groupId = "group_id")
    public void listenCustomer(Integer customerId) {
        log.info("Kafka : Début réception du clients {} pour le supprimer des commandes", customerId);
        try {
            orderRepository.deleteAllByCustomerId(customerId);
        } catch (Exception e) {
            log.error("Impossible de supprimer les commandes qui possèdent le client : {}", customerId);
        }
        log.info("Kafka : Fin de suppression des commandes pour le client : {}", customerId);
    }
}
