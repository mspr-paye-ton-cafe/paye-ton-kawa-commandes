package com.payetoncafe.commandes.service;

import com.payetoncafe.commandes.events.OrderProducer;
import com.payetoncafe.commandes.exception.OrderException;
import com.payetoncafe.commandes.model.Customer;
import com.payetoncafe.commandes.model.OrderWithProducts;
import com.payetoncafe.commandes.model.Orders;
import com.payetoncafe.commandes.model.Product;
import com.payetoncafe.commandes.repository.OrderRepository;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClient;

import java.util.ArrayList;
import java.util.List;


@Service
public class OrderService {

    @Inject
    OrderRepository orderRepository;

    @Inject
    OrderProducer orderProducer;

    private static final Logger log = LoggerFactory.getLogger(OrderService.class);

    RestClient restClient = RestClient.create();

    public OrderWithProducts getOrderById(Integer id) throws OrderException {
        Orders orders = orderRepository.findById(id).orElseThrow(() -> new OrderException("Could not find order with id " + id));
        return mapOrderToOrderWithProducts(orders);
    }

    public List<OrderWithProducts> getOrders() throws OrderException {
        List<Orders> orders = orderRepository.findAll();
        return orders.stream().map(this::mapOrderToOrderWithProducts).toList();
    }

    public void deleteOrder(Integer id) throws OrderException {
        orderRepository.deleteById(id);
    }

    @Transactional
    public Orders createOrder(Orders orders) {
        Orders ordersToCreate = orderRepository.save(orders);
        if (orders.getProductIds() != null && !orders.getProductIds().isEmpty()) {
            orderProducer.sendOrderStockEvent(orders.getProductIds());
        } else {
            log.error("kafka : No product IDs to send");
        }
        return ordersToCreate;
    }

    public Orders updateOrder(Orders orders) throws OrderException {
        Orders orderToUpdate = orderRepository.findById(orders.getId()).orElseThrow(() -> new OrderException("Could not find order with id " + orders.getId()));
        orderToUpdate.id(orders.getId());
        orderToUpdate.createdAt(orders.getCreatedAt());
        orderToUpdate.customerId(orders.getCustomerId());
        orderToUpdate.productIds(orders.getProductIds());
        return orderRepository.save(orderToUpdate);
    }

    private OrderWithProducts mapOrderToOrderWithProducts(Orders orders) {
        List<Product> products = callProductApiWithProductIdsList(orders.getProductIds());
        Customer customer = callCustomerApiWithCustomerId(orders.getCustomerId());
        return new OrderWithProducts()
                .id(orders.getId())
                .createdAt(orders.getCreatedAt())
                .customerId(orders.getCustomerId())
                .customer(customer)
                .products(products);
    }

    protected List<Product> callProductApiWithProductIdsList(List<Integer> productIds) {
        List<Product> productList = new ArrayList<>();
        productIds.forEach(productId -> {
                    Product product = restClient.get()
                            .uri("http://localhost:8082/products/{id}", productId)
                            .retrieve()
                            .onStatus(HttpStatusCode::is4xxClientError, (request, response) -> {
                                log.info("Error retrieve calling product : " + productId + ", error code  : " + response.getStatusCode());})
                            .toEntity(Product.class).getBody();
                    if (product != null && product.getId() != null) {
                        productList.add(product);
                    }
                }
        );
        log.info("product list: " + productList);
        return productList;
    }

    protected Customer callCustomerApiWithCustomerId(Integer customerId) {
        Customer customer = new Customer();
        customer = restClient.get()
                .uri("http://localhost:8080/customers/{id}", customerId)
                .retrieve()
                .onStatus(HttpStatusCode::is4xxClientError, (request, response) -> {
                    log.info("Error retrieve calling product : " + customerId + ", error code  : " + response.getStatusCode());})
                .toEntity(Customer.class).getBody();
        return customer;
    }

}