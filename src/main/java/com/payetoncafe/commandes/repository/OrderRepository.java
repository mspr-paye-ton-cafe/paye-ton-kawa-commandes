package com.payetoncafe.commandes.repository;

import com.payetoncafe.commandes.model.Orders;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Integer> {

    @Transactional
    void deleteAllByCustomerId(Integer customerId);

}
