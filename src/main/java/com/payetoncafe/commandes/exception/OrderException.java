package com.payetoncafe.commandes.exception;

public class OrderException extends Throwable {
    public OrderException(String message){
        super(message);
    }
}
