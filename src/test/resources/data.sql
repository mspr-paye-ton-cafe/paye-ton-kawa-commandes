insert into orders (id, created_at, customer_id) values (1, '2023-08-30T10:54:50',1);
insert into orders (id, created_at, customer_id) values (2, '2024-08-30T10:54:50',1);
insert into orders (id, created_at, customer_id) values (3, '2024-08-30T10:54:50',1);
insert into orders (id, created_at, customer_id) values (4, '2024-08-30T10:54:50',55);

insert into orders_product_ids (orders_id, product_ids) values (1,2);
insert into orders_product_ids (orders_id, product_ids) values (1,3);
insert into orders_product_ids (orders_id, product_ids) values (2,2);
insert into orders_product_ids (orders_id, product_ids) values (2,3);
insert into orders_product_ids (orders_id, product_ids) values (2,13);
insert into orders_product_ids (orders_id, product_ids) values (2,14);

insert into orders_product_ids (orders_id, product_ids) values (3,9);
insert into orders_product_ids (orders_id, product_ids) values (3,13);
insert into orders_product_ids (orders_id, product_ids) values (3,11);