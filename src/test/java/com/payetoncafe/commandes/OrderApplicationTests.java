package com.payetoncafe.commandes;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.mockStatic;

@SpringBootTest
class OrderApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void testMainMethod() {
		try (var mockedSpringApplication = mockStatic(SpringApplication.class)) {
			OrderApplication.main(new String[]{});
			mockedSpringApplication.verify(() -> SpringApplication.run(OrderApplication.class, new String[]{}));
		}
	}
}
