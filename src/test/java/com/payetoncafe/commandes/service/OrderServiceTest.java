package com.payetoncafe.commandes.service;

import com.payetoncafe.commandes.events.OrderProducer;
import com.payetoncafe.commandes.exception.OrderException;
import com.payetoncafe.commandes.model.Customer;
import com.payetoncafe.commandes.model.OrderWithProducts;
import com.payetoncafe.commandes.model.Orders;
import com.payetoncafe.commandes.model.Product;
import com.payetoncafe.commandes.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClient;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class OrderServiceTest {
    @Mock
    RestClient restClient;
    @Mock
    OrderRepository orderRepository;
    @InjectMocks
    OrderService orderService;

    @Mock
    private OrderProducer orderProducer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetOrderById() throws OrderException {
        RestClient.ResponseSpec responseSpecMock = productRestClientMock();

        customerRestClientMock(responseSpecMock);

        when(orderRepository.findById(0)).thenReturn(Optional.of(new Orders()));

        OrderWithProducts result = orderService.getOrderById(Integer.valueOf(0));
        Assertions.assertEquals(new OrderWithProducts().customer(new Customer()), result);

        verify(orderRepository).findById(any());
    }

    @Test
    void testGetOrders() throws OrderException {
        RestClient.ResponseSpec responseSpecMock = productRestClientMock();

        customerRestClientMock(responseSpecMock);
        when(orderRepository.findAll()).thenReturn(List.of(new Orders()));

        List<OrderWithProducts> result = orderService.getOrders();
        Assertions.assertEquals(1, result.size());

        verify(orderRepository).findAll();
    }

    @Test
    void testDeleteOrderById() throws OrderException {
        when(orderRepository.findAll()).thenReturn(List.of(
                new Orders(),
                new Orders()
        ));
        when(orderRepository.findById(2)).thenReturn(Optional.of(new Orders()));
        when(orderRepository.findAll()).thenReturn(List.of(new Orders()));

        orderService.deleteOrder(2);

        verify(orderRepository).deleteById(any());
    }

    @Test
    void testCallCustomerApi(){
        RestClient.ResponseSpec responseSpecMock = getResponseSpecMock();
        customerRestClientMock(responseSpecMock);
        Customer customer = orderService.callCustomerApiWithCustomerId(1);

        Assertions.assertEquals(1, customer.getId());
    }

    @Test
    void testCallProductApiWithProductIdsListOk() {
        productRestClientMock();

        List<Integer> productIds = Arrays.asList(2, 3);
        List<Product> products = orderService.callProductApiWithProductIdsList(productIds);

        Assertions.assertEquals(2, products.size());
    }

    @Test
    void testCallProductApiWithProductIdsListNotFound() {
        RestClient.ResponseSpec responseSpecMock = getResponseSpecMock();
        ResponseEntity<Product> responseEntityMock = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        when(responseSpecMock.toEntity(Product.class)).thenReturn(responseEntityMock);

        List<Integer> productIds = Arrays.asList(2,5);
        List<Product> products = orderService.callProductApiWithProductIdsList(productIds);

        Assertions.assertEquals(0, products.size());
    }

    @Test
    void testCallProductApiWithProductIdsListNotFoundProductNull() {
        RestClient.ResponseSpec responseSpecMock = getResponseSpecMock();
        Product mockProduct = mock(Product.class);
        when(mockProduct.getId()).thenReturn(null);
        ResponseEntity<Product> responseEntityMock = new ResponseEntity<>(mockProduct, HttpStatus.OK);
        when(responseSpecMock.toEntity(Product.class)).thenReturn(responseEntityMock);

        List<Integer> productIds = Arrays.asList(1,2);
        List<Product> products = orderService.callProductApiWithProductIdsList(productIds);

        Assertions.assertEquals(0, products.size());
    }


    @Test
    void testUpdateOrder() throws OrderException {
        when(orderRepository.findById(any())).thenReturn(Optional.ofNullable(new Orders().id(1).createdAt(OffsetDateTime.MAX).customerId(1).productIds(List.of(1, 2))));
        Orders updatedOrder = new Orders().id(1).createdAt(OffsetDateTime.MAX).customerId(2).productIds(List.of(1, 2));
        when(orderRepository.save(any(Orders.class))).thenReturn(updatedOrder);

        Orders updateOrder = orderService.updateOrder(updatedOrder);
        Assertions.assertEquals(2, updateOrder.getCustomerId());

        verify(orderRepository).findById(any());
        verify(orderRepository).save(any());
    }

    @Test
    void testCreateOrder() {
        Orders order = new Orders();
        List<Integer> productIds = Arrays.asList(1, 2, 3);
        order.setProductIds(productIds);

        Orders savedOrder = new Orders();
        when(orderRepository.save(order)).thenReturn(savedOrder);

        Orders result = orderService.createOrder(order);

        Assertions.assertEquals(savedOrder, result);
        verify(orderRepository).save(order);
        verify(orderProducer).sendOrderStockEvent(productIds);
    }

    @Test
    void testCreateOrderProductListEmpty() {
        Orders order = new Orders();
        order.setProductIds(Collections.emptyList());

        Orders savedOrder = new Orders();
        when(orderRepository.save(order)).thenReturn(savedOrder);

        Orders result = orderService.createOrder(order);

        Assertions.assertEquals(savedOrder, result);
        verify(orderRepository).save(order);
        verify(orderProducer, never()).sendOrderStockEvent(anyList());
    }

    @Test
    void testCreateOrderProductListNull() {
        Orders order = new Orders();
        order.setProductIds(null);

        Orders savedOrder = new Orders();
        when(orderRepository.save(order)).thenReturn(savedOrder);

        Orders result = orderService.createOrder(order);

        Assertions.assertEquals(savedOrder, result);
        verify(orderRepository).save(order);
        verify(orderProducer, never()).sendOrderStockEvent(anyList());
    }


    private RestClient.ResponseSpec getResponseSpecMock() {
        RestClient.RequestHeadersUriSpec requestHeadersUriSpecMock = mock(RestClient.RequestHeadersUriSpec.class);
        when(restClient.get()).thenReturn(requestHeadersUriSpecMock);
        when(requestHeadersUriSpecMock.uri(anyString(), Optional.ofNullable(any()))).thenReturn(requestHeadersUriSpecMock);
        RestClient.ResponseSpec responseSpecMock = mock(RestClient.ResponseSpec.class);
        when(requestHeadersUriSpecMock.retrieve()).thenReturn(responseSpecMock);
        when(responseSpecMock.onStatus(any(), any())).thenReturn(responseSpecMock);
        return responseSpecMock;
    }

    private RestClient.ResponseSpec productRestClientMock() {
        RestClient.ResponseSpec responseSpecMock = getResponseSpecMock();
        Product mockProduct = mock(Product.class);
        ResponseEntity<Product> responseEntityMock = new ResponseEntity<>(mockProduct, HttpStatus.OK);
        when(responseSpecMock.toEntity(Product.class)).thenReturn(responseEntityMock);
        return responseSpecMock;
    }

    private static void customerRestClientMock(RestClient.ResponseSpec responseSpecMock) {
        Customer mockCustomer = mock(Customer.class);
        ResponseEntity<Customer> responseEntityMockCustomer = new ResponseEntity<>(mockCustomer, HttpStatus.OK);
        when(responseSpecMock.toEntity(Customer.class)).thenReturn(responseEntityMockCustomer);
        when(mockCustomer.getId()).thenReturn(1);
    }


}