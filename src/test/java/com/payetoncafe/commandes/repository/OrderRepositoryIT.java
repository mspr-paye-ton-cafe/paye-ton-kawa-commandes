package com.payetoncafe.commandes.repository;
import static org.assertj.core.api.Assertions.assertThat;

import com.payetoncafe.commandes.model.Orders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderRepositoryIT {

    @Autowired
    OrderRepository orderRepository;

    @Test
    void testDeleteByCustomerId() {
        // When
        orderRepository.deleteAllByCustomerId(55);

        // Then
        List<Orders> remainingOrders = orderRepository.findAll();
        assertThat(remainingOrders).hasSize(3);
    }
}
