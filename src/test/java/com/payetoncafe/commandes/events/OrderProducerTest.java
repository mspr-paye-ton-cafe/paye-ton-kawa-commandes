package com.payetoncafe.commandes.events;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class OrderProducerTest {

    @Mock
    private KafkaTemplate<String, List<Integer>> kafkaTemplate;

    @InjectMocks
    private OrderProducer orderProducer;

    @Test
    void testSendOrderStockEvent() {
        // Given
        List<Integer> productIds = Arrays.asList(1, 2, 3);

        // When
        orderProducer.sendOrderStockEvent(productIds);

        // Then
        verify(kafkaTemplate, times(1)).send(OrderProducer.TOPIC, productIds);
    }
}
