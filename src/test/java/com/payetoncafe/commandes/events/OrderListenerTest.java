package com.payetoncafe.commandes.events;

import com.payetoncafe.commandes.repository.OrderRepository;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OrderListenerTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderListener orderListener;

    @Test
    void testListenCustomer() {
        // Given
        Integer customerId = 1;

        // When
        orderListener.listenCustomer(customerId);

        // Then
        verify(orderRepository, times(1)).deleteAllByCustomerId(customerId);
    }

    @Test
    void testListenCustomerKo() {
        // Given
        Integer customerId = 1;
        doThrow(new RuntimeException("Database error")).when(orderRepository).deleteAllByCustomerId(customerId);

        // When
        orderListener.listenCustomer(customerId);

        // Then
        verify(orderRepository, times(1)).deleteAllByCustomerId(customerId);

    }
}
