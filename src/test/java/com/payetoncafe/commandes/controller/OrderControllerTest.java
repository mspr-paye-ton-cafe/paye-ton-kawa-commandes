package com.payetoncafe.commandes.controller;

import com.payetoncafe.commandes.exception.OrderException;
import com.payetoncafe.commandes.model.OrderWithProducts;
import com.payetoncafe.commandes.model.Orders;
import com.payetoncafe.commandes.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.mockito.Mockito.*;

class OrderControllerTest {
    @Mock
    OrderService orderService;
    @InjectMocks
    OrderController orderController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateCommand() {
        ResponseEntity<Orders> result = orderController.createOrder(new Orders());
        Assertions.assertEquals(200, result.getStatusCode().value());
    }

    @Test
    void testDeleteCommande() {
        ResponseEntity<Void> result = orderController.deleteOrder(Integer.valueOf(0));
        Assertions.assertEquals(200, result.getStatusCode().value());
    }

    @Test
    void testDeleteCommandeKo() throws OrderException {
        doThrow(new OrderException("error")).when(orderService).deleteOrder(1);
        ResponseStatusException commandException = Assertions.assertThrows(ResponseStatusException.class, () -> orderController.deleteOrder(Integer.valueOf(1)));
        Assertions.assertEquals(204, commandException.getStatusCode().value());
    }

    @Test
    void testGetAllOrder() throws OrderException {
        when(orderService.getOrders()).thenReturn(List.of(new OrderWithProducts()));

        ResponseEntity<List<OrderWithProducts>> result = orderController.getAllOrders();
        Assertions.assertEquals(1, result.getBody().size());
        Assertions.assertEquals(200, result.getStatusCode().value());
    }

    @Test
    void testGetAllOrdersThrowException() throws OrderException {
        when(orderService.getOrders()).thenThrow(new OrderException("error"));
        ResponseStatusException commandException = Assertions.assertThrows(ResponseStatusException.class, () -> orderController.getAllOrders());
        Assertions.assertEquals(204, commandException.getStatusCode().value());
    }


    @Test
    void testGetOrderById() throws OrderException {
        when(orderService.getOrderById(anyInt())).thenReturn(new OrderWithProducts());

        ResponseEntity<OrderWithProducts> result = orderController.getOrderById(Integer.valueOf(0));

        Assertions.assertEquals(new OrderWithProducts(), result.getBody());
        Assertions.assertEquals(200, result.getStatusCode().value());
    }

    @Test
    void testGetOrderByIdThrowException() throws OrderException {
        when(orderService.getOrderById(anyInt())).thenThrow(new OrderException("error"));
        ResponseStatusException orderException = Assertions.assertThrows(ResponseStatusException.class, () -> orderController.getOrderById(Integer.valueOf(0)));
        Assertions.assertEquals(404, orderException.getStatusCode().value());
    }

    @Test
    void testUpdateOrderOk() {
        ResponseEntity<Orders> result = orderController.updateOrder(new Orders());
        Assertions.assertEquals(200, result.getStatusCode().value());
    }

    @Test
    void testUpdateOrderKo() throws OrderException {
        when(orderService.updateOrder(any())).thenThrow(new OrderException("error"));
        ResponseStatusException exception = Assertions.assertThrows(ResponseStatusException.class, () -> orderController.updateOrder(new Orders()));
        Assertions.assertEquals(204, exception.getStatusCode().value());
    }
}